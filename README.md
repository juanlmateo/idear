iDEAR - iDamID Enrichment Analysis with R
=========================================

Description
-----------

Identify the binding profile of a transcription factor (or any DNA binding protein) from iDamIDseq data. Different to other methods, with iDamIDseq + **iDEAR** you obtain both the regions in the genome where your protein of interest (POI) is present or bound and also the regions from where your POI is actively or passively excluded. The first set of regions is evidenced by significantly higher methylation signal due to the activity of the Dam protein fused to the POI with respect to the control sample. The second set corresponds to the regions with significantly higher methylation signal by the fusion of the Dam protein to GFP+NLS.

If you use this tool for you scientific work, please cite it as:

Gutierrez-Triana JA, Mateo JL, Ibberson D, Ryu S, Wittbrodt J (2016) iDamIDseq and iDEAR: an improved method and computational pipeline to profile chromatin-binding proteins. Development 143:4272–4278.

For more information you can read our manuscript in Development: [iDamIDseq and iDEAR: an improved method and computational pipeline to profile chromatin-binding proteins](http://dev.biologists.org/content/143/22/4272) or in biorxiv: [iDamIDseq and iDEAR: An improved method and a computational pipeline to profile chromatin-binding proteins of developing organisms](http://biorxiv.org/content/early/2016/07/05/062208)

Installation
------------

### From bitbucket

You can install **iDEAR** directly from bitbucket using the devtools package. To do that run these commands:

``` r
if(!require("devtools")) install.packages("devtools")
devtools::install_bitbucket("juanlmateo/idear")
```

Usage
-----

### Samples information

First of all, you need to define the information of the samples to be analysed as a data.frame with three columns. The first column holds the name of the sample (POI or GFP), the second the replicate number and the third the path to the bam file with the mapped reads for each sample.

``` r
samples <- data.frame(name = factor(c("TF","TF","GFP","GFP"),levels = c("TF","GFP")),
                      replicate = c(1,2,1,2),
                      paths = c("/path/to/TF_replicate1.bam","/path/to/TF_replicate2.bam",
                                "/path/to/GFP_replicate1.bam","/path/to/GFP_replicate2.bam"))
```

In order to determine which factor is used in the numerator and denominator for the DE analysis and interpretation of the log2FC there are two possibilities: if the first column in the `samples` variable, identifying the sample name, is of factor type the order of the levels is taken into account, i.e. first level will be the numerator and the second level the denominator. In other case, the first listed sample will be in the numerator and the second in the denominator. In the example above the sample name is defined as a factor and the order, first "TF", means that the "TF" sample is considered as positive and "GFP" as control.

### "GATC" fragments

It is necessary to have the coordinates of the fragments in the genome flanked by "GATC" sites, potential DpnI or DpnII restriction sites. You may have these coordinates in a bed file, in this case provide the path to this file directly to the function `getEnrichedDamRegions`. Otherwise **iDEAR** provides the function `getDpnIFragments` to extract these coordinates from a `BSgenome` object.

``` r
library(BSgenome.Olatipes.UCSC.oryLat2)
fragments <- getDpnIFragments(BSgenome.Olatipes.UCSC.oryLat2)
```

If there is not yet a package for the genome of your species it is simple to create a BSgenome object from a fasta file with the genome sequence following the instructions from this [vignette](https://bioconductor.org/packages/release/bioc/vignettes/BSgenome/inst/doc/BSgenomeForge.pdf).

### Enrichment analysis

Once the samples information and the fragments coordinates are ready you only need to use the function `getEnrichedDamRegions`. This function computes the read counts for all the selected fragments. Fragments with very low read coverage are excluded based on the value of the parameter `min.rel.count`. Read coverage is defined as the ratio of the number of reads in each fragment to its length. This ratio must be greater than `min.rel.count` times the ratio of total reads in fragments to the length of all fragments.

Next, from the fragments that pass this filter those that are adjacent, or separated by a distance smaller than the length of the shorter fragment, will be join together to form sites. Finally, using the DESeq2 package this function defines the sites from the genome with significant difference between the samples.

``` r
results <- getEnrichedDamRegions(samples,fragments)
```

As mentioned above, the value for the parameter `fragments` can be the path to a bed file or the result of the function `getDpnIFragments`.

After running the previous snippet the `results` variable contains the coordinates of the identified regions as a GRanges object with additional columns. The column `log2FoldChange` indicates the fold change of POI versus the control in log2. Taking into account this column it is possible to separate the "positive", or bound, and "negative" regions if necessary.

``` r
poi.positive <- results[mcols(results)$log2FoldChange > 0, ]
poi.negative <- results[mcols(results)$log2FoldChange < 0, ]
```

### Visualization

Optionally, for visualization purposes, you can use the function `saveBigWigScore` that will generate a bigWig file with the log2 fold change of the average normalized read coverage between the POI and the control replicates. The resulting file can be displayed, for instance, in the [UCSC genome brower](http://genome.ucsc.edu/) or tools like [IGV](http://www.broadinstitute.org/software/igv/).

``` r
saveBigWigScore(samples,BSgenome.Olatipes.UCSC.oryLat2,'myAnalysis.bw')
```
